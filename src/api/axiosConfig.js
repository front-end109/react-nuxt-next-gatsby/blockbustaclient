import axios from 'axios';

export default axios.create({
    baseURL:'https://b957-41-150-224-31.eu.ngrok.io',
    headers: {"ngrok-skip-browser-warning": "true"}
});